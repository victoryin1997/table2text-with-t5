import pandas as pd
import os

class DataPreProcessor:

    def __init__(self, path, dataset_name = "valid"):
        self.path = path
        self.dataset_name = dataset_name

    def is_field_value_pair_undesired(self, field, value):
        return field.find('image') >= 0 or field.find('caption') >= 0 or value == '<none>'


    def construct_info_box(self, line):
        field_value_pairs = line.split('\t')
        output = ""
        for token in field_value_pairs:
            current_info_box = {}
            field = token.split(':')[0]
            value = token.split(':')[1]
            value = value.replace('\n', '')
            if self.is_field_value_pair_undesired(field, value):
                continue
            field_name = field[:field.rfind('_')]
            output += field_name + ':' + value + '#'
        return output.rstrip('#').rstrip('\n')

    def main(self):
        file_path = os.path.join(self.path, self.dataset_name)
        nb_path = os.path.join(file_path, self.dataset_name+".nb")
        label_path = os.path.join(file_path, self.dataset_name+".sent")
        feature_path = os.path.join(file_path, self.dataset_name+".box")

        # Load line number
        line_number_indicators = []
        with open(nb_path) as file:
          line_number_indicators = [line.rstrip("\n") for line in file]

        # Load infobox
        infobox = []
        with open(feature_path) as file:
          line = file.readline()
          counter = 0
          while line:
            infobox += [self.construct_info_box(line)]
            line = file.readline()
            counter += 1

        # Load sentence
        sentences = []
        with open(label_path) as file:
          start_ind = 0
          lines = file.readlines()
          for lines_to_read in line_number_indicators:
            end_ind = start_ind + int(lines_to_read)
            sentences += ["".join(lines[start_ind: end_ind]).replace("\n", "")]
            start_ind = end_ind
        assert(len(sentences) == len(line_number_indicators))
        assert(len(sentences) == len(infobox))


        df = pd.DataFrame({'infobox': infobox, 'bio': sentences})
        output_path = os.path.join(self.file_path, self.dataset_name+".csv")
        df.to_csv(output_path, index=False)