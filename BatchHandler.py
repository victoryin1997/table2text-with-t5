import random
import torch
from torch.nn.utils.rnn import pad_sequence


class BatchHandler:
    def __init__(self, logger, tokenizer):
        self.logger = logger
        self.tokenizer = tokenizer

    def collate(self, batch):
        src_text = []
        tgt_text = []
        for sample in batch:
            src_text.append(sample[0])
            tgt_text.append(sample[1])
            if random.randint(1, 10001) <= 2:
                self.logger.info("Random Infobox/Bio string test")
                self.logger.info("   Infobox = %s", sample[0])
                self.logger.info("   Bio = %s", sample[1])
        assert (len(src_text) == len(tgt_text))
        data_set = self.tokenizer.prepare_seq2seq_batch(src_texts=src_text, tgt_texts=tgt_text, padding="longest").data
        examples = [[torch.tensor(data_set["input_ids"][ind]), torch.tensor(data_set["labels"][ind])]
                    for ind in range(len(data_set["input_ids"]))]

        text = [item[0] for item in examples]
        label = [item[1] for item in examples]
        if self.tokenizer._pad_token is None:
            text = pad_sequence(text, batch_first=True)
            label = pad_sequence(label, batch_first=True)
            return [text, label]
        text = pad_sequence(text, batch_first=True, padding_value=self.tokenizer.pad_token_id)
        label = pad_sequence(label, batch_first=True, padding_value=self.tokenizer.pad_token_id)
        return [text, label]
