import pandas as pd
import os
from pandas import DataFrame
from transformers import AutoModelForSeq2SeqLM, AutoTokenizer
import torch

device = torch.device("cuda" if (torch.cuda.is_available()) else "cpu")

def load_examples_and_label(test_data_path):
    src_text = []
    label_text = []
    data_samples = pd.read_csv(test_data_path).values.tolist()

    for sample in data_samples:
        src_text.append(sample[0])  # test without prefix
        label_text.append(sample[1])
    samples = [[samp, tgr_t] for samp, tgr_t in zip(src_text, label_text)]

    return samples

def generate(model_path: str, test_data_path: str, batch_size: int, output_path: str):
    tokenizer = AutoTokenizer.from_pretrained(model_path)
    model = AutoModelForSeq2SeqLM.from_pretrained(model_path)

    samples = load_examples_and_label(test_data_path)
    model.to(device)

    columns_list = ["sample", "label"]
    for i in range(0, 5):
        columns_list.append("generation_" + str(i))
    output_df_format = DataFrame(columns=columns_list)

    num_return_sequences = 5
    for i in range(0, len(samples), batch_size):
        sample = [sam[0] for sam in samples[i: i + batch_size]]
        labels = [sam[1] for sam in samples[i: i + batch_size]]

        inputs = tokenizer.batch_encode_plus(sample, return_tensors="pt", padding=True)
        generation_output = model.generate(inputs["input_ids"].to(device),
                                           min_length=100,
                                           max_length=1024,
                                           do_sample=True,
                                           top_p=0.9, top_k=50,
                                           bos_token_id=tokenizer.bos_token_id,
                                           eos_token_id=tokenizer.eos_token_id,
                                           pad_token_id=tokenizer.pad_token_id,
                                           num_return_sequences=num_return_sequences,
                                           early_stopping=True)

        generation = tokenizer.batch_decode(generation_output, skip_special_tokens=True)
        start = 0
        generation_list = []
        for n in sample:
            for m in range(start, start + num_return_sequences):
                generation_list.append(generation[m].strip())
            start = start + num_return_sequences
            # for n in sample:
            #     for m in range(start, start + opt.num_return_sequences):
            #         generation_list.append(generation[m].replace(n.replace(tokenizer.bos_token, '').replace(tokenizer.additional_special_tokens[0], ''),"").strip())
            #     start = start + opt.num_return_sequences

        initial = 0
        for ins, label in zip(sample, labels):
            df_temp = DataFrame(index=[0],columns=columns_list)
            df_temp["sample"] = ins
            df_temp["label"] = label
            for i in range(initial, initial + num_return_sequences):
                df_temp["generation_"+str(i % num_return_sequences)] = generation_list[i]
            initial = initial + num_return_sequences
            output_df_format = output_df_format.append(df_temp, ignore_index=True)
        write_into_csv(output_path, output_df_format)
        output_df_format = DataFrame(columns=columns_list)

def write_into_csv(output_path, df):
    append = False
    if os.path.isfile(output_path):
        append = True
    df.to_csv(output_path, index=False, mode='a' if append else 'w', header=not append)