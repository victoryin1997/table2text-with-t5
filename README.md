## Installation
`pip install -r requirements.txt`
---
## Dataset link
The preprocessed dataset was generated from *data_generator.ipynb*:
https://drive.google.com/drive/folders/1wIbPrfdg4OUkh9xEqlCwUuPt0orFT5GC?usp=sharing
---
## How to run
### Before run
It is expected that a user to review configurations in `finetune_t5_configs.ini` each time before the run.
### Start to run
For the best practice, run the following in a script at the root of the repo:

    import finetune_t5
    from configparser import ConfigParser
    import json
    
    config = ConfigParser()
    config.read('finetune_t5_configs.ini')
    paras = json.loads(config.get("eval", "paras"))
    finetune_t5.main(paras)
Tweak `config.get("eval", "paras")` to adjust between train/eval/test mode