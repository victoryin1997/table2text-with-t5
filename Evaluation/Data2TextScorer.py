import math
import t5.evaluation.metrics as t5
from Evaluation import metrics as metrics


def divide_chunks(l, n):
    # looping till length l
    for i in range(0, len(l), n):
        yield l[i:i + n]


def get_bleu_result(candidates, label):
    # BLEU score
    max_bleu_score = 0.0
    best_index = 0
    for i in range(len(candidates)):
        # BLEU
        bleu_analysis = t5.bleu([label], [candidates[i]])
        if max_bleu_score < bleu_analysis['bleu']:
            max_bleu_score = bleu_analysis['bleu']
            best_index = i

    return {"bleu_pred": candidates[best_index], "bleu_score": round(max_bleu_score,3)}


# ROUGE score
def get_rouge_result(candidates, label):

    max_rouge_score = 0.0
    best_rouge_index = 0

    for i in range(len(candidates)):
        # ROUGE
        rouge_analysis = metrics.rouge([label], [candidates[i]])
        rouge_score = rouge_analysis['rougeLsum']
        if max_rouge_score < rouge_score:
            max_rouge_score = rouge_score
            best_rouge_index = i
    return {"rouge_pred": candidates[best_rouge_index], "rouge_score": round(max_rouge_score,3)}


def get_readability_result(predictions, target_text):
    target_readability = metrics.get_readability(target_text)['flesch_kincaid']
    best_readability_index = 0
    best_readability = 0.0
    min_delta = math.inf
    for i in range(len(predictions)):
        current_readability = metrics.get_readability(predictions[i])['flesch_kincaid']
        current_delta = abs(current_readability - target_readability)
        if (current_delta < min_delta):
            best_readability = current_readability
            best_readability_index = i
    return {"readability_pred": predictions[best_readability_index], "readability_score": round(best_readability, 3)}


def corpus_score(input_text, target_text, predictions):
    results = {"input": input_text, "label": target_text}
    results.update(get_bleu_result(predictions, target_text))
    results.update(get_rouge_result(predictions, target_text))
    results.update(get_readability_result(predictions, target_text))

    return results


def batch_score(inputs, targets, predictions):
    results = []
    for i in range(len(predictions)):
        results += [corpus_score(inputs[i], targets[i], predictions[i])]

    return results