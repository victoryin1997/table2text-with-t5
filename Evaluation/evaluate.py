from Evaluation import Data2TextScorer as scorer
import pandas as pd
import os

def batch_generate(input_path: str, output_path: str, logging_steps: int):
    samples, labels = load_examples_and_label(input_path)
    predictions = load_predictions(input_path)
    for i in range(0, len(samples), logging_steps):
        results = []
        score_result = scorer.batch_score(samples[i:i+logging_steps], labels[i:i+logging_steps], predictions[i:i+logging_steps])
        results.extend(score_result)
        write_into_csv(output_path, results)


def load_examples_and_label(test_data_path):
    src_text = []
    label_text = []
    data_samples = pd.read_csv(test_data_path).values.tolist()
    for sample in data_samples:
        src_text.append(sample[0])
        label_text.append(sample[1])

    return src_text, label_text

def load_predictions(pred_path):
    data = pd.read_csv(pred_path).values.tolist()
    predictions = []
    for row in data:
        # Skip input and label columns
        prediction = row[2:]
        prediction_list = []
        prediction_list_2 = []
        for i in prediction:
            prediction_list.append(str(i))
        for i in prediction_list:
            if i == 'nan':
                prediction_list_2.append("")
            else:
                prediction_list_2.append(i)
        predictions.append(prediction_list_2)
    return predictions


def write_into_csv(output_path, results):
    df = pd.DataFrame(results)
    append = False
    if os.path.isfile(output_path):
        append = True
    df.to_csv(output_path, index=False, mode='a' if append else 'w', header=not append)


if __name__ == '__main__':
    batch_generate(
        input_path="predictions.csv",
        output_path="evaluate_results.csv",
        logging_steps=5
    )
